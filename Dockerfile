FROM ubuntu
COPY executable/GeniussportsHelloWorld.exe /bin/

RUN apt-get -y update
RUN apt-get -y install mono-complete

CMD ["mono","/bin/GeniussportsHelloWorld.exe"]

