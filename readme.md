# Welcome to the hello world project!

Clone this repository or download the zip file

THis project is based on cake build script. By running build.ps1 script, it will do following:

  - It will start building executable (.exe) from solution file.
  - Once it will finish build process, executable file will be placed into executable folder in cake root folder
  - Then, script will create nuget folder and executable will be packaged into nuget package and placed into this folder. 
  - Last step, script will call Dockerfile to start building a container. It will download the ubuntu image, will run update and will install mono and will execute our .exe file.
  - Once you will run: Docker run vytis:image it will show you - Good day World! and will close in few seconds.
 
After running build.ps1, all folders(executable and nuget) will be generated in cake root folder


