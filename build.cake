#addin nuget:?package=Cake.Docker

var target = Argument("target", "Build");

Task("Default")
  .IsDependentOn("Directory")
  .IsDependentOn("Pack")
  .IsDependentOn("Docker");

Task("Build")
    .Does(() =>
{
    MSBuild("./helloWorldGeniusS/GeniussportsHelloWorld.sln", new MSBuildSettings()
      .WithProperty("OutputPath", "../../executable"));
});

Task("Directory")
  .IsDependentOn("Build")
  .Does(() =>
{
    EnsureDirectoryExists("nuget");
});



Task("Pack")
 .IsDependentOn("Build")
  .Does(() => {
    var nuGetPackSettings   = new NuGetPackSettings {
                                    Id                      = "GeniusSportsHelloWorld",
                                    Version                 = "0.0.0.1",
                                    Title                   = "Hello World",
                                    Authors                 = new[] {"Vytis Vaiksnoras"},
                                    Description             = "Some description here",
                                    Summary                 = "Some summary",
                                    Files                   = new [] {
                                                                        new NuSpecContent {Source = "GeniussportsHelloWorld.exe"},
                                                                      },
                                    BasePath                = "./executable/",
                                    OutputDirectory         = "./nuget"
                                };

    NuGetPack(nuGetPackSettings);
  });
  
Task("Docker")
.Does(() => {
    var settings = new DockerBuildSettings { Tag = new[] {"vytis:image" }};
    DockerBuild(settings, ".");
});

  RunTarget(target);


